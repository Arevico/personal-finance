<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth as Auth;
use DB as DB;
class Transaction extends Model
{

    protected $guarded = ['id'];
    
    public function user(){
       return $this->hasOne('App\User');
    }

    /**
     *
     */
   public function categories( ){
       $categories = DB::select(
        '       SELECT 
                    MONTH(FROM_UNIXTIME(t.date)) as month, c.name, 
                    sum(amount/100) 
                FROM transactions t 
                RIGHT JOIN categories c ON c.id = t.category_id WHERE t.user_id= ?
                GROUP BY MONTH(FROM_UNIXTIME(t.date)), c.name
                ORDER BY t.date DESC ',  
       [
            Auth::id() 
       ]);

       return $categories;
    }

    public function getDebits(){
       $money = DB::select(
        'SELECT concat(
                YEAR(FROM_UNIXTIME(t.date)), \' \',
                MONTHNAME(FROM_UNIXTIME(t.date))
                ) as y, 
                sum(case when amount > 0 then amount/100 else 0 end) as credits,
                sum(case when amount < 0 then abs(amount/100) else 0 end) as debits,
                sum(amount) as total
                FROM transactions t 
                WHERE t.user_id= ? GROUP BY YEAR(FROM_UNIXTIME(t.date)), MONTH(FROM_UNIXTIME(t.date))
                ORDER BY t.date DESC
                LIMIT 100',  
       [
            Auth::id() 
       ]);  
       $money =array_reverse($money);
       return $money;
    }


    public function spentOnFood(){
       $categories = DB::select(
        '       SELECT 
                    (FROM_UNIXTIME(t.date)) as month,
                    ABS(sum(amount/100)) as total
                FROM transactions t 
                JOIN categories c ON c.id = t.category_id WHERE t.user_id= ? AND  c.name IN ("Voedsel", "Pizza", "pizza", "Junkfood")
                GROUP BY MONTH(FROM_UNIXTIME(t.date))
                ORDER BY t.date DESC ',  
       [
            Auth::id() 
       ]);

       return array_reverse($categories);
    }


    public function getThisMonth(){

    }

}
    

    