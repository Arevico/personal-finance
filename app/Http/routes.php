<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


// Allias for multiple
Route::auth();

Route::get('/home', 'HomeController@index');

// All protected routes
Route::group(['middleware' => ['auth'] ], function(){
   
    // Dashboard Routes

    // Transaction Routes
    Route::get('/app/transacties/toevoegen', ['uses' => 'TransactionController@add', 'as' => 'TransactionsAdd' ]);
    Route::post('/app/transacties/toevoegen', ['uses' => 'TransactionController@processAdd']);

    Route::get('/app/transacties', ['uses' => 'TransactionController@overview', 'as' => 'TransactionsOverview' ]);
    Route::get('/app', ['uses' => 'DashboardController@overview', 'as' => 'DashboardOverview'] );
});


Route::get('/', function () {
    return view('welcome');
});

